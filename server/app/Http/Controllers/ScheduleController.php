<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $schedules = Schedule::all();
        return response()->json($schedules);
    }

    /**
     * Get the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $schedule = Schedule::where('id', $id)->get();
        if(!empty($schedule['items'])){
            return response()->json($schedule);
        }
        else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'deviceId' => 'required',
            'startTime' => 'required',
            'endTime' => 'required',
        ]);

        $schedule = new Schedule();
        $schedule->deviceId = $request->deviceId; //'73qy7t889umcrq289jmjuia1j';
        $schedule->startTime = $request->startTime; //06:00';
        $schedule->endTime = $request->endTime; //'08:30';
        $schedule->save();
        return response()->json(['status' => 'success']);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'deviceId' => 'required',
            'startTime' => 'required',
            'endTime' => 'required',
        ]);

        $schedule = Schedule::find($id);
        $schedule->deviceId = $request->deviceId; //'73qy7t889umcrq289jmjuia1j';
        $schedule->startTime = $request->startTime; //06:00';
        $schedule->endTime = $request->endTime; //'08:30';
        $schedule->save();
        return response()->json(['status' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Books::destroy($id)){
             return response()->json(['status' => 'success']);
        }
    }
}

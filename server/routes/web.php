<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['prefix' => 'schedule/', 'middleware' => 'auth'], function ($app) {
    $app->get('/','ScheduleController@index'); //get all the routes	
    $app->post('/','ScheduleController@store'); //store single route
    $app->get('/{id}/', 'ScheduleController@show'); //get single route
    $app->put('/{id}/','ScheduleController@update'); //update single route
    $app->delete('/{id}/','ScheduleController@destroy'); //delete single route
});

$app->get('/', function () use ($app) {
    return "API running";
});

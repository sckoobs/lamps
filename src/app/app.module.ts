import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import {
  LoginPage,
  TabsPage,
  HomePage,
  SchedulePage,
  DevicePage, EditDevicePage,
  EditSchedulePage,
  SettingsPage
} from '../pages/pages';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SwitchServiceProvider } from '../providers/switch-service/switch-service';
import { DeviceProvider } from '../providers/device/device';
import { AuthProvider } from '../providers/auth/auth';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
//import { AngularFireDatabase } from 'angularfire2/database';


const firebaseConfig = {
  apiKey: "AIzaSyCHY-BJSIlEieq4CqJ_APe3NHMBcGzq6x8",
  authDomain: "switches-app-83a7b.firebaseapp.com",
  databaseURL: "https://switches-app-83a7b.firebaseio.com",
  projectId: "switches-app-83a7b",
  storageBucket: "switches-app-83a7b.appspot.com",
  messagingSenderId: "846199692560"
};

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    DevicePage,
    EditDevicePage,
    SchedulePage,
    EditSchedulePage,
    SettingsPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    DevicePage,
    EditDevicePage,
    SchedulePage,
    EditSchedulePage,
    SettingsPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SwitchServiceProvider,
    DeviceProvider,
    AuthProvider
  ]
})
export class AppModule {}

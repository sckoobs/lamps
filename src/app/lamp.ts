export enum LampStatus {
  Unkown = -1,
  Off,
  On  
}

export class Lamp {
  status: LampStatus;
  processing: boolean;
  constructor(status: LampStatus) {
    this.status = status;
  }
  toggle(status) {
    switch (status) {
      case LampStatus.Off: this.status = LampStatus.Off;
        break;
      case LampStatus.On: this.status = LampStatus.On;
        break;
      case LampStatus.Unkown:
      default:
        return;
    }
  }
  getState() {
    switch (this.status) {
      case LampStatus.Off: return 'Off';
      case LampStatus.On: return 'On';
      case LampStatus.Unkown:
      default:
        return 'Unknown';
    }
  }
  getOppositeState() {
    switch (this.status) {
      case LampStatus.Off: return 'On';
      case LampStatus.On: return 'Off';
      case LampStatus.Unkown:
      default:
        return 'Unknown';
    }
  }
}
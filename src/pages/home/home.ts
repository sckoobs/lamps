import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';

import { SwitchServiceProvider } from '../../providers/switch-service/switch-service';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';

import { AuthProvider } from '../../providers/auth/auth';

import IDevice, { Device } from '../device/device';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [SwitchServiceProvider]
})
export class HomePage {
  itemsCollection: AngularFirestoreCollection<IDevice>;
  items: Observable<IDevice[]>;
  processing: boolean;
  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public authData: AuthProvider,
    public switchService: SwitchServiceProvider,
    public db: AngularFirestore) {
  }
  ionViewDidLoad() {
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });

    var user = this.authData.getCurrentUser();
    loader.present().then(() => {
      this.itemsCollection = this.db.collection<IDevice>('devices');
      this.items = this.itemsCollection.snapshotChanges().map(actions => {
        return actions.map(a => {
          var data = { id: a.payload.doc.id, ...a.payload.doc.data() } as IDevice;
          var device = new Device(data.id, data.accountId, data.deviceId, data.name);
/*
make call to get the device status
          this.switchService.status(data.deviceId)
            .then(result => {
              device.status = result;
            })
*/

          return device;
        })
      });
      loader.dismiss();
    });
  }
  toggleLamp(lamp) {
    lamp.processing = true;
    this.switchService.toggle(lamp.deviceId)
      .then(result => {
        lamp.toggle(result);
        lamp.processing = false;
      }, result => {
        console.error("Error toggling lamp: ", result);
        lamp.processing = false;
      });
  }
}

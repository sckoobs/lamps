import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import { Observable } from 'rxjs';

import { AuthProvider } from '../../providers/auth/auth';

import { EditSchedulePage } from '../pages';

import Schedule from './schedule';

@Component({
  selector: 'page-schedule',
  templateUrl: 'list.html'
})
export class SchedulePage {
  itemsCollection: AngularFirestoreCollection<Schedule>;
  items: Observable<Schedule[]>;
  constructor(
    public nav: NavController,
    public loadingCtrl: LoadingController,
    public authData: AuthProvider,
    public db: AngularFirestore) {
  }
  ionViewDidLoad() {
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });

    var user = this.authData.getCurrentUser();
    loader.present().then(() => {
      this.itemsCollection = this.db.collection('userschedules');
      this.items = this.itemsCollection.valueChanges();
      loader.dismiss();
    });
  }  
  openCreateView(e) {
    this.nav.push(EditSchedulePage, { mode: "Create" });
  }
  openEditView(item) {
    this.nav.push(EditSchedulePage, { mode: "Edit", items: this.items, item: item });
  }
  remove() {
    
  }
}

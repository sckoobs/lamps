import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import { Observable } from 'rxjs';

import Schedule from './schedule';

@Component({
  selector: 'page-edit-schedule',
  templateUrl: 'edit.html',
})
export class EditSchedulePage {
  action: string;
  
  itemsCollection: AngularFirestoreCollection<Schedule>;
  items: Observable<Schedule[]>;
  item: Schedule;
  key: string;

  entryForm: FormGroup;

  startTime: string;
  endTime: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder
  ) {
    this.action = navParams.data.mode;
    this.items = navParams.data.items;

    this.item = navParams.data.item;
    if (this.item) {
      this.startTime = this.item.startTime;
      this.endTime = this.item.endTime;
    }
    
    this.entryForm = formBuilder.group({
      startTime: ['', Validators.compose([
        Validators.pattern("[0-9]{2}\:[0-9]{2}"),
        Validators.required
      ])],
      endTime: ['', Validators.compose([
        Validators.pattern("[0-9]{2}\:[0-9]{2}"),
        Validators.required
      ])]
    });
  }

  submit(){
    if (!this.entryForm.valid) {
      console.log(this.entryForm.value);
    } else {
      var obj = {
        startTime: this.entryForm.controls.startTime.value,
        endTime: this.entryForm.controls.endTime.value
      };

      if (this.item) {
        //this.items.update(this.item.$key, obj);
      } else {
        //this.items.push(obj);
      }

      this.navCtrl.pop();
    }
  }
}

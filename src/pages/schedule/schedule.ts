interface Schedule {
    id: string;
    startTime: string;
    endTime: string;
}

export default Schedule;
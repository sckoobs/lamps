import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';

import Device from './device';

@IonicPage()
@Component({
  selector: 'page-edit-device',
  templateUrl: 'edit.html',
})
export class EditDevicePage {
  action: string;

  itemsCollection: AngularFirestoreCollection<Device>;
  item: any;
  key: string;

  entryForm: FormGroup;

  name: string;
  deviceId: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public db: AngularFirestore
  ) {
    this.action = navParams.data.mode;
    this.itemsCollection = navParams.data.items;

    this.item = navParams.data.item;
    if (this.item) {
      this.name = this.item.name;
      this.deviceId = this.item.deviceId;
    }

    this.entryForm = formBuilder.group({
      name: ['', Validators.compose([
        Validators.minLength(3),
        Validators.required
      ])],
      deviceId: ['', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])]
    });
  }

  submit() {
    if (!this.entryForm.valid) {
      console.log(this.entryForm.value);
    } else {
      if (this.item) {
        this.db.doc<Device>(`devices/${this.item.id}`).update(this.item);
      } else {
        const id = this.db.createId();
        const item: Device = {
          id,
          accountId: 'kyuR0IHzmXVzzMiehYgM',
          name: this.entryForm.controls.name.value,
          deviceId: this.entryForm.controls.deviceId.value
        };
        this.itemsCollection.add(item);
      }

      this.navCtrl.pop();
    }
  }
}

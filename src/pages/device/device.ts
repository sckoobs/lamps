interface IDevice {
  id: string;
  accountId: string;
  deviceId: string;
  name: string;
}

export enum DeviceStatus {
  Unkown = -1,
  Off,
  On
}

export class Device implements IDevice {
  status: DeviceStatus;
  processing: boolean;

  constructor(
    public id: string,
    public accountId: string,
    public deviceId: string,
    public name: string
  ) {
  }
  toggle(status) {
    switch (status) {
      case DeviceStatus.Off: this.status = DeviceStatus.Off;
        break;
      case DeviceStatus.On: this.status = DeviceStatus.On;
        break;
      case DeviceStatus.Unkown:
      default:
        return;
    }
  }
  getState() {
    switch (this.status) {
      case DeviceStatus.Off: return 'Off';
      case DeviceStatus.On: return 'On';
      case DeviceStatus.Unkown:
      default:
        return 'Unknown';
    }
  }
  getOppositeState() {
    switch (this.status) {
      case DeviceStatus.Off: return 'On';
      case DeviceStatus.On: return 'Off';
      case DeviceStatus.Unkown:
      default:
        return 'Unknown';
    }
  }
}

export default IDevice;

import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import { Observable } from 'rxjs';

import { AuthProvider } from '../../providers/auth/auth';

import { EditDevicePage } from './edit';

import Device from './device';

@IonicPage()
@Component({
  selector: 'page-device',
  templateUrl: 'list.html',
})
export class DevicePage {
  itemsCollection: AngularFirestoreCollection<Device>;
  items: Observable<Device[]>;
  constructor(
    public nav: NavController,
    public loadingCtrl: LoadingController,
    public authData: AuthProvider,
    public db: AngularFirestore) {
  }
  ionViewDidLoad() {
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });

    var user = this.authData.getCurrentUser();
    loader.present().then(() => {
      this.itemsCollection = this.db.collection<Device>('devices');
      this.items = this.itemsCollection.valueChanges();
      loader.dismiss();
    });
  }
  openCreateView(e) {
    this.nav.push(EditDevicePage, { mode: "Create", items: this.itemsCollection });
  }
  openEditView(item) {
    this.nav.push(EditDevicePage, { mode: "Edit", items: this.itemsCollection, item: item });
  }
  remove() {
    
  }
}

import { Component } from '@angular/core';

import { HomePage, SchedulePage, DevicePage } from '../pages';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = SchedulePage;
  tab3Root = DevicePage;

  constructor() {

  }
}

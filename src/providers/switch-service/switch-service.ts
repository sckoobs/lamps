import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SwitchServiceProvider {
  data: object;
  deviceId: string;
  token: string;
  constructor(public http: Http) {
    this.data = null;
    //this.deviceId = "55ff73065075555323351487";
    this.token = "5535d923be6c7befdcb867594805c21bcd5e9dd2";
  }
  status(deviceId: string){
    return new Promise(resolve => {
        this.http.get(`https://api.particle.io/v1/devices/${deviceId}/status?access_token=${this.token}`)
          .map(res => res.json())
          .subscribe(data => {
            this.data = data.result;
            resolve(this.data);
          });
      });
  }
  switch(deviceId: string, onOrOff: number){
    return new Promise(resolve => {
      const body = { access_token: this.token, args: onOrOff == 1 ? "on" : "off" };
      this.http.post(`https://api.particle.io/v1/devices/${deviceId}/switch`, body)
          .map(res => res.json())
          .subscribe(data => {
            this.data = data.return_value;
            resolve(this.data);
          });
      });
  }
  toggle(deviceId: string){
    return new Promise(resolve => {
      let parms = new URLSearchParams();
      parms.append("access_token", this.token);
      this.http.post(`https://api.particle.io/v1/devices/${deviceId}/toggle`, parms)
          .map(res => res.json())
          .subscribe(data => {
            this.data = data.return_value;
            resolve(this.data);
          });
      });
  }
}
